class ProductModel {
    final description;
    final id;
    final images;
    final price;
    final rating;
    final title;

    ProductModel.fromJson(Map<String,dynamic> json)
    :   description=json["description"],
        id=json["id"],
        images=json["images"],
        price=json["price"],
        rating=json["rating"],
        title=json["title"];
}