

import 'package:flutter/material.dart';
import 'package:treinetic_app/model/product_model.dart';
import 'package:treinetic_app/services/products_api.dart';
class ProductDetailsProvider extends ChangeNotifier{
    bool loading = true;
    ProductModel? model;
    
    ProductDetailsProvider(ProductModel? model){
        this.model=model;
        init();
    }

    init()async{
        if(model==null){
            model = await ProductAPI().details(); 
        }
        endLoading();
    }

    startLoading(){
        loading =true;
        notifyListeners();
    }

    endLoading(){
        loading =false;
        notifyListeners();
    }

}
