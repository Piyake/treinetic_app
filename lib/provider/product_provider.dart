

import 'package:flutter/material.dart';
import 'package:treinetic_app/model/product_model.dart';
import 'package:treinetic_app/services/products_api.dart';
class ProductProvider extends ChangeNotifier{
    bool loading = true;
    List<ProductModel> products=[];
    
    ProductProvider(){
        init();
    }

    init()async{
        products = await ProductAPI().get(); 
        endLoading();
    }

    startLoading(){
        loading =true;
        notifyListeners();
    }

    endLoading(){
        loading =false;
        notifyListeners();
    }

}
