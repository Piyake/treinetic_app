import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:treinetic_app/model/product_model.dart';
import 'package:treinetic_app/util/common_consts.dart';

class ProductAPI {
    Future<List<ProductModel>> get() async {
        try {
            var uri = Uri.https(CommonConstants.baseUrl, '/products');
            final response = await http.get(
                uri,
                headers: {
                    'Content-Type': 'application/json'
                }
            );
            var jsonData =  json.decode(response.body.toString());
            return List.from(jsonData.map((e)=>ProductModel.fromJson(e)));
        }catch(e){
            throw e;
        }
    }

    Future<ProductModel> details() async {
        try {
            var uri = Uri.https(CommonConstants.baseUrl, '/featured');
            final response = await http.get(
                uri,
                headers: {
                    'Content-Type': 'application/json'
                }
            );
            var jsonData =  json.decode(response.body.toString());
            return ProductModel.fromJson(jsonData);
        }catch(e){
            throw e;
        }
    }

    
}