
import 'package:flutter/material.dart';
import 'package:treinetic_app/views/detail_view.dart';
import 'package:treinetic_app/views/home_view.dart';
import 'routing_contsants.dart';

Route<dynamic> generateRoute(RouteSettings settings){
    switch (settings.name) {
        case RouteNames.Home:
            return MaterialPageRoute(builder: (context)=>HomeView(),settings: settings);    
        case RouteNames.Details:
            Map<String,dynamic> args = settings.arguments as Map<String,dynamic>;
            return MaterialPageRoute(builder: (context)=>DetailsView(model: args['model']),settings: settings);
        default:
            return MaterialPageRoute(builder: (context)=>HomeView(),settings: settings);
    }
}
