
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:treinetic_app/provider/product_provider.dart';
import 'package:treinetic_app/widgets/category_ribbon.dart';
import 'package:treinetic_app/widgets/featured_card.dart';
import 'package:treinetic_app/widgets/header_caption.dart';
import 'package:treinetic_app/widgets/product_card.dart';

class HomeView extends StatefulWidget {
  const HomeView({ Key? key }) : super(key: key);

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
    @override
    Widget build(BuildContext context) {
        return ChangeNotifierProvider<ProductProvider>(
            create:(_)=>ProductProvider(),
            child: Consumer<ProductProvider>(
                builder: (context,productProvider,child){
                    return SafeArea(
                        child: Scaffold(
                            appBar: AppBar(
                                elevation: 0,
                                backgroundColor: Colors.white,
                                leading: Icon(Icons.menu,color:Colors.black87),
                                centerTitle: true,
                                actions: [
                                    Padding(
                                      padding: const EdgeInsets.only(right:15.0),
                                      child: Icon(Icons.search,color:Colors.black87),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(right:25.0),
                                      child: Icon(Icons.shopping_bag_outlined,color:Colors.black87),
                                    )
                                ],
                            ),
                            body: Stack(
                                children: [
                                    Container(
                                        margin: EdgeInsets.only(top:5,left: 25,bottom: 55),
                                        child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                                HeaderCaption(),
                                                FeaturedCard(),
                                                CategoryRibbon(),
                                                if(productProvider.loading)
                                                    Container(
                                                        child: Center(
                                                            child: SpinKitThreeBounce(
                                                                color: Colors.blue,
                                                                size:18,
                                                            )
                                                        ),
                                                    )
                                                else
                                                    Expanded(
                                                        child: ListView.builder(
                                                            shrinkWrap: true,
                                                            itemCount: productProvider.products.length,
                                                            scrollDirection: Axis.horizontal,
                                                            itemBuilder: (BuildContext context, int index){
                                                                return ProductCard(model:productProvider.products[index]);
                                                            }
                                                        )
                                                    )
                                            ],
                                        ),
                                    ),
                                    Positioned(
                                        bottom: 0,
                                        child : Container(
                                            width: MediaQuery.of(context).size.width,
                                            padding:EdgeInsets.all(5),
                                            child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: [
                                                    Icon(Icons.home_outlined,color:Theme.of(context).primaryColor),
                                                    Icon(Icons.send_outlined,color:Colors.grey),
                                                    Icon(Icons.favorite_border,color:Colors.grey),
                                                    Icon(Icons.person_outline,color:Colors.grey)
                                                ]
                                            ),
                                        )
                                    )
                                ],
                            ),
                        ),
                    );
                }
            ),
        );
    }
}