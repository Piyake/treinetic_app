import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:treinetic_app/model/product_model.dart';
import 'package:treinetic_app/provider/product_details_%20provider.dart';
import 'package:treinetic_app/widgets/feature_button.dart';
import 'package:treinetic_app/widgets/rating_row.dart';

class DetailsView extends StatefulWidget {
    final ProductModel? model;
    const DetailsView({this.model});

    @override
    _DetailsViewState createState() => _DetailsViewState();
}

class _DetailsViewState extends State<DetailsView> {
    @override
    Widget build(BuildContext context) {
        return ChangeNotifierProvider<ProductDetailsProvider>(
            create:(_)=>ProductDetailsProvider(widget.model),
            child: Consumer<ProductDetailsProvider>(
                builder: (context,details,child){
                    return SafeArea(
                        child: Scaffold(
                            appBar: AppBar(
                                elevation: 0,
                                leading:IconButton(icon:Icon(Icons.arrow_back,color:Colors.white), onPressed: () { Navigator.pop(context);}),
                                centerTitle: true,
                                actions: [
                                    Padding(
                                      padding: const EdgeInsets.only(right:25.0),
                                      child: Icon(Icons.shopping_bag_outlined,color:Colors.white),
                                    )
                                ],
                            ),
                            body:Container(
                                height: MediaQuery.of(context).size.height,
                                decoration: BoxDecoration(
                                    color: Theme.of(context).primaryColor
                                ),
                                child: details.loading
                                ?Container(
                                    child: Center(
                                        child: SpinKitThreeBounce(
                                            color: Colors.white,
                                            size:18,
                                        )
                                    ),
                                )
                                : Column(
                                    crossAxisAlignment: CrossAxisAlignment.stretch,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                        Expanded(
                                          child: Container(
                                              child: Image.network(
                                                  details.model!.images
                                              ),
                                          ),
                                        ),
                                        Container(
                                            height: 350,
                                            width: MediaQuery.of(context).size.width,
                                            padding: EdgeInsets.symmetric(horizontal: 25,vertical: 15),
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius: BorderRadius.only(
                                                    topLeft: Radius.circular(45),
                                                    topRight: Radius.circular(45)
                                                )
                                            ),
                                            child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                                children: [
                                                    Row(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        children: [
                                                            Container(
                                                                height: 4,
                                                                width:50,
                                                                decoration: BoxDecoration(
                                                                    color: Colors.grey[300]
                                                                ),
                                                                child: SizedBox(width:50),
                                                            ),
                                                        ],
                                                    ),
                                                    SizedBox(height:15),
                                                    Text(
                                                        details.model!.title,
                                                        style: Theme.of(context).textTheme.bodyText2!.copyWith(
                                                            fontSize: 16,
                                                            fontWeight: FontWeight.bold
                                                        ),
                                                        maxLines: 2,
                                                    ),
                                                    Text(
                                                        details.model!.price,
                                                        style: Theme.of(context).textTheme.headline6!.copyWith(
                                                            fontWeight: FontWeight.bold
                                                        ),
                                                        textAlign: TextAlign.end,
                                                    ),
                                                    ProductRatingRow(rating: details.model!.rating),
                                                    SizedBox(height:5),
                                                    Text(details.model!.description,style: TextStyle(fontWeight: FontWeight.w400,fontSize: 12,height: 1.5),maxLines: 5,),
                                                    SizedBox(height:12),
                                                    Row(
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                        children: [
                                                            FeatureButton(icon: Icons.remove_red_eye_outlined, feature: 'Improved Optics'),
                                                            FeatureButton(icon: Icons.brightness_6_rounded, feature: 'Clear Brightness'),
                                                            FeatureButton(icon: Icons.wifi, feature: 'Wifi Controllers')
                                                        ],
                                                    ),
                                                    SizedBox(height:14),
                                                    ElevatedButton(
                                                        child: Text('CheckOut',style: TextStyle(color:Colors.white)),
                                                        style: ElevatedButton.styleFrom(
                                                            primary:Colors.grey[800],
                                                            elevation: 0
                                                        ),
                                                        onPressed:(){}
                                                    )
                                                ],
                                            ),
                                        )
                                    ],
                                ),
                            ),
                        ),
                    );
                }
            ),
        );
    }
}