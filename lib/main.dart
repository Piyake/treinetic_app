import 'package:flutter/material.dart';
import 'routes/router.dart' as router;

void main() {
    runApp(
         MyApp()
    );
}

class MyApp extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
      return MaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(
                    primarySwatch: Colors.blue,
                    scaffoldBackgroundColor: Colors.white
            ),
            initialRoute:'/',
            onGenerateRoute:router.generateRoute,
        );
    }
}

