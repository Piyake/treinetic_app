import 'package:flutter/material.dart';

class FeatureButton extends StatelessWidget {
    final IconData icon;
    final String feature;

  const FeatureButton({required this.icon, required this.feature});

    @override
    Widget build(BuildContext context) {
        return Container(
            padding: EdgeInsets.all(5),
            child: Column(
                children: [
                    Container(
                        padding: EdgeInsets.all(3),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(color: Colors.grey)
                        ),
                        child: Icon(icon, color: Colors.grey[700])),
                    SizedBox(height: 3,),
                    Text(feature,style: TextStyle( color: Colors.grey[700],fontWeight: FontWeight.w400,fontSize: 11))
                ],
            ),
        );
    }
}