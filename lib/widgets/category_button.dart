import 'package:flutter/material.dart';

class CategoryButton extends StatelessWidget {
    final String text;
    final VoidCallback  onPressed;
    final bool isActive;
    const CategoryButton({required this.text, required this.onPressed, required this.isActive });

    @override
    Widget build(BuildContext context) {
        return Container(
            child: Container(
                margin: EdgeInsets.only(right:10),
                child: ElevatedButton(
                    child: Text(text,style: TextStyle(color:isActive?Colors.white:Colors.black)),
                    style: ElevatedButton.styleFrom(
                        primary:isActive?Theme.of(context).primaryColor:Colors.grey[400],
                        elevation: 0
                    ),
                    onPressed:onPressed
                ),
            ),
        );
    }
}