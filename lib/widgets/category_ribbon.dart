import 'package:flutter/material.dart';
import 'package:treinetic_app/widgets/category_button.dart';

class CategoryRibbon extends StatelessWidget {
    const CategoryRibbon({ Key? key }) : super(key: key);

    @override
    Widget build(BuildContext context) {
        return Container(
            padding: EdgeInsets.symmetric(vertical: 8),
            child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                    children: [
                        CategoryButton(text: 'All Products', onPressed: (){}, isActive: true),
                        CategoryButton(text: 'Popular', onPressed: (){}, isActive: false),
                        CategoryButton(text: 'Recent', onPressed: (){}, isActive: false),
                        CategoryButton(text: 'Recommended', onPressed: (){}, isActive: false)
                    ],
                ),
            ),
        );
    }
}