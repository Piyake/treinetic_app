import 'package:flutter/material.dart';

class ProductRatingRow extends StatelessWidget {
    final double rating;

    const ProductRatingRow({required this.rating});

    @override
    Widget build(BuildContext context) {
        return Container(
            height: 30,
            child: Row(
                children: [
                    ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: 5,
                        itemBuilder: (BuildContext context, int index){
                            return Container(
                                padding:EdgeInsets.only(right: 5),
                                child: Icon(
                                    Icons.star,
                                    size: 15,
                                    color: rating>=index+1?Colors.amber[600]:Colors.grey,
                                ),
                            );
                        },
                    ),
                    Text('5',style: TextStyle(fontWeight: FontWeight.w700,fontSize: 12 )),
                    SizedBox(width:5),
                    Text('(28 reviews)',style: TextStyle(fontWeight: FontWeight.w400,fontSize: 12)),
                    SizedBox(width:15),
                ],
            ),
        );
    }
}