import 'package:flutter/material.dart';
import 'package:treinetic_app/routes/routing_contsants.dart';

class FeaturedCard extends StatelessWidget {
    const FeaturedCard();

    @override
    Widget build(BuildContext context) {
        return InkWell(
            child: Container(
                padding: EdgeInsets.only(top:20,bottom:20,right:25),
                child: Stack(
                    clipBehavior: Clip.none,
                    children: [
                        Container(
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                color: Theme.of(context).primaryColor,
                                borderRadius: BorderRadius.circular(20)
                            ),
                            padding: EdgeInsets.symmetric(vertical: 20,horizontal: 15),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                    Text(
                                        'Improved Controller \nDesign with \nVirtual Reality',
                                        style: Theme.of(context).textTheme.headline6!.copyWith(
                                            height: 1.5,
                                            color:Colors.white
                                        ),
                                        maxLines: 3,
                                    ),
                                    SizedBox(
                                        height: 12,
                                    ),
                                    ElevatedButton(
                                        child: Text('Buy Now!',style: TextStyle(color: Theme.of(context).primaryColor)),
                                        style: ElevatedButton.styleFrom(
                                            primary: Colors.white, 
                                        ),
                                        onPressed: (){}
                                    )
                                ],
                            ),
                        ),
                        Positioned(
                            top: 20,
                            right: -40,
                            child: Container(
                                height: 170,
                                child: Image.network(
                                    'https://i.postimg.cc/g0KRMTzK/Pico-G2-4k-hero2-234.png'
                                ),
                            ),
                        )
                    ],
                ),
            ),
            onTap: (){
                Navigator.of(context).pushNamed(RouteNames.Details,arguments: {"model":null});
            },
        );
    }
}