import 'package:flutter/material.dart';

class HeaderCaption extends StatelessWidget {
  const HeaderCaption({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Text(
            'Enjoy the world \ninto virtual reality',
            style: Theme.of(context).textTheme.headline6!.copyWith(
                height: 1.5
            ),
            maxLines: 2,
        ),
    );
  }
}