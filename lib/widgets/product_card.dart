import 'package:flutter/material.dart';
import 'package:treinetic_app/model/product_model.dart';
import 'package:treinetic_app/routes/routing_contsants.dart';

class ProductCard extends StatelessWidget {
    final ProductModel model;

    const ProductCard({required this.model});

    @override
    Widget build(BuildContext context) {
        return InkWell(
            child: Container(
                clipBehavior: Clip.antiAlias,
                width: 140,
                margin: EdgeInsets.only(right:10),
                decoration: BoxDecoration(
                    color: Colors.grey[100],
                    borderRadius: BorderRadius.circular(15),
                ),
                child: Stack(
                    children: [
                        Container(
                            child: Container(
                                padding: EdgeInsets.all(10),
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                        Container(
                                            height: 150,
                                            padding: EdgeInsets.symmetric(vertical:15),
                                            child:Image.network(
                                                model.images
                                            )
                                        ),
                                        Text(
                                            model.title,
                                            style:Theme.of(context).textTheme.bodyText2!.copyWith(
                                                fontWeight: FontWeight.w600
                                            )
                                        ),
                                        SizedBox(height: 10),
                                        Text(
                                            model.price,
                                            style:Theme.of(context).textTheme.bodyText2!.copyWith(
                                                fontSize: 11,
                                                fontWeight: FontWeight.w300
                                            )
                                        )
                                    ],
                                ),
                            ),
                        ),
                        Positioned(
                            right:0,
                            bottom: 0,
                            child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(6),
                                    color: Colors.grey[800]
                                ),
                                padding: EdgeInsets.all(6),
                                child: Icon(
                                    Icons.add,
                                    color: Colors.white,
                                ),
                            ),
                        )
                    ],
                ),
            ),
            onTap: (){
                Navigator.of(context).pushNamed(RouteNames.Details,arguments: {"model":model});
            },
        );
    }
}